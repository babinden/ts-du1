import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FaktorialTest {
    @Test
    public void factorialTest() {
        Faktorial faktorial = new Faktorial();
        int n = 3;
        long expectedValue = 6;

        long result = faktorial.factorial(n);

        Assertions.assertEquals(expectedValue, result);
    }
}